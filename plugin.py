###
# Copyright (c) 2021, Glandos
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import unicodedata
import marshal
import re

from pathlib import Path

from supybot import utils, plugins, ircutils, callbacks
from supybot.commands import *

try:
    from supybot.i18n import PluginInternationalization

    _ = PluginInternationalization("UnicodeDatabase")
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


# TODO: make this configurable
MAX_LISTED_RESULTS = 5
CODE_POINT_RE = re.compile(r"(?:(?:u|U)\+)?(?:(?P<code>[0-9a-fA-F]{4,8}))")
CATEGORY_INVERT_COLORS = [
    "Cc",
    "Cf",
    "Cn",
    "Co",
    "Cs",
    "Mc",
    "Me",
    "Mn",
    "Zl",
    "Zp",
    "Zs",
]


class UnicodeDatabase(callbacks.Plugin):
    """Query Unicode database"""

    def build_unicode(self, file):
        data = []
        for i in range(0x110000):
            char = chr(i)
            name = unicodedata.name(char, "")
            if name:
                data.append(name)
        marshal.dump(frozenset(data), file)
        return unicodedata.unidata_version

    def find_name(self, db, name):
        return [match for match in db if name in match]

    def get_code_point(self, char):
        return "U+{0:0>4}".format(hex(ord(char))[2:].upper())

    def get_char_repr(self, char):
        return "{circle}{char}{circle}".format(
            circle="" if unicodedata.combining(char) == 0 else "\u202F", char=char
        )

    def unicode_find(self, irc: callbacks.IrcObjectProxy, search):
        """
        Find all Unicode name that matches
        """
        db_search = search.upper()
        with Path(__file__).parent.joinpath("unicode.db").open("rb") as file:
            db = marshal.load(file)
            names = self.find_name(db, db_search)
            if names:

                def colorize(name):
                    return name.replace(
                        db_search, ircutils.mircColor(db_search, fg="orange")
                    )

                names.sort()
                prefix = "- "
                result_count = len(names)
                if result_count == 1:
                    name = colorize(names[0])
                    self.unicode_info(irc, unicodedata.lookup(names[0]), name)
                elif result_count <= MAX_LISTED_RESULTS:
                    irc.reply("{} results".format(result_count))
                    for name in names:
                        char = unicodedata.lookup(name)
                        self.unicode_info(irc, char, colorize(name), prefix)
                else:
                    results = ", ".join(colorize(name) for name in names)
                    irc.reply("{} results".format(result_count))
                    irc.reply(results)
            else:
                irc.reply("No match for {}".format(search))

    def unicode_info(self, irc: callbacks.IrcObjectProxy, char, name=None, prefix=""):
        invert_colors = unicodedata.category(char) in CATEGORY_INVERT_COLORS
        irc.reply(
            "{prefix}{name} ({char} {escaped})".format(
                prefix=prefix,
                name=name if name else unicodedata.name(char, "<No name>"),
                char=ircutils.mircColor(
                    self.get_char_repr(char),
                    fg="black" if invert_colors else None,
                    bg="white" if invert_colors else None,
                ),
                escaped=self.get_code_point(char),
            )
        )

    def unicode(self, irc: callbacks.IrcObjectProxy, msg, args, query):
        """
        Query the Unicode database.

        Use either a single character to get it's codepoint, a code point to get the character,
        or a full string that will be looked up in descriptions.
        """
        if len(query) == 1:
            self.unicode_info(irc, query)
            return

        code_point_matcher = CODE_POINT_RE.fullmatch(query)
        if code_point_matcher:
            value = code_point_matcher.group("code")
            self.unicode_info(irc, chr(int(value, 16)))
        else:
            self.unicode_find(irc, query)

    unicode = wrap(unicode, [first("letter", "text")])

    def rebuild(self, irc: callbacks.IrcObjectProxy, msg, args):
        """
        Rebuilt the Unicode name database
        """
        with Path(__file__).parent.joinpath("unicode.db").open("w+b") as file:
            version = self.build_unicode(file)
            irc.reply(f"Database rebuilt with Unicode {version}")

    rebuild = wrap(rebuild, ["admin"])


Class = UnicodeDatabase


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
